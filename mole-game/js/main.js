var board = document.querySelector(".game_area");
var scoreBoard = document.querySelector(".score");


// var seed = new Date();
// var date = seed.getSeconds();
// console.log((60 - date)/15);
// var boxes = Math.floor((60 - date)/15  + 5);

var date = new Date();
var s = date.getSeconds()%10;
console.log("s "+(60-s)/60);
console.log("Math "+ Math.floor(  (  Math.random()*4  )*  (  10-s  )  /10  )  );
var boxes = ((Math.floor((Math.random()+4)*(10-s)/10))+5)>8?8:((Math.floor((Math.random()+4)*(10-s)/10))+5);
console.log("box "+boxes);

let timeCounter = false;
let clickable = -1;
var click = 0;
let score = 0;


class moleBox{
    constructor(id){
        this.createHoles(id);
    }

    createHoles(id){
        var hole = document.createElement("div");
        hole.classList.add("hole");
        hole.setAttribute("id", id);
        
        var mole = document.createElement('img');
        mole.classList.add("mole");
        mole.src = "mole.png";
        hole.addEventListener("click", stabMole);


        var dirt = document.createElement('img');
        dirt.classList.add('dirt');
        dirt.src = "dirt.png";

        hole.appendChild(mole);
        hole.appendChild(dirt);

        board.appendChild(hole);

    }
}


function randomHole(){
    var box = Math.floor(Math.random() * boxes);
    
    var rd = board.childNodes[box];
    clickable = rd.id;
    // console.log(clickable);
    return rd;
}


function randomMoleUp(){

    
    rdhole = randomHole();
    rdhole.querySelector(".mole").style.bottom = "15px";

    // console.log(rdhole)
    // console.log(rdhole.querySelector(".mole").style.bottom);

    setTimeout(function() {

        rdhole.querySelector(".mole").style.bottom = "-100px";
        //console.log("time: "+timeCounter);
        if(!timeCounter){
            randomMoleUp();
        }
    }, 1000);

    //console.log(timeCounter);
}

function stabMole(){

    if(clickable == this.getAttribute("id")){
        score++;
        scoreBoard.textContent = "Score: " + score;
        moleDown(this);
    }
}

function moleDown(obj){
    console.log(obj);
    obj.childNodes[0].style.bottom = "-100px";
}

function restartGame(){
    window.location.reload();
    // /window.onload = randomBoxes;
}

function randomBoxes(){
    
    // document.querySelector(".refresh").classList.add("header");
    // document.querySelector(".refresh").classList.remove(".refresh");

    scoreBoard.innerHTML = "Score : 0";
    timeCounter = false;
    score = 0;
    board.innerHTML = "";
    for(let i=0;i<boxes;i++){
        new moleBox(i);
    }
        
    randomMoleUp();

    setTimeout(() => timeCounter = true, 10000);
}


